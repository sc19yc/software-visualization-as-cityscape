import { scene } from './scene/index.js'
import { renderer, camera } from './RendererCamera.js'
import * as THREE from './three.js-r123/build/three.module.js'

var mouse;
mouse = new THREE.Vector2();
//listen mouse event
document.addEventListener('mousemove', onDocumentMouseMove, false);

//mousemove function
function onDocumentMouseMove(event) {
  event.preventDefault();
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

}

function render() {

  renderer.render(scene, camera); //render
  requestAnimationFrame(render); //Rerender

  }

render();


export {renderer,render}
