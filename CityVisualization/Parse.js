var data = $.parseJSON($.ajax({
    url: "AnalysisInfo.json",
    dataType: "json", 
    async: false
}).responseText);

// data is an array of dict
// every element of data be like :
// {classesnum : ,pkgname :, classes:[]}
// classes is an array of dict too
// every element of data["classes"] be like
//{classname: ,linenum: ,methodnum: }
//
//[{
    // classesnum:1,
    
    // pkgname: "...",
    // classes:[{
    //     methodnum:,
    //     linenum:,
    //     classname:,
    //     comment:
    // },
    // {}]},
//   {  classesnum:
//...
//}]

//Total number of pkg: 

// pkg1name(bigger size):
// class num of this pkg:
//   -class1name:
//   comment of this class(if exists):
//   linenum:
//   methodnum:

//   -class2name:
//   comment of this class(if exists):
//   linenum:
//   methodnum:

//   -class3name:
//   comment of this class(if exists):
//   linenum:
//   methodnum:

// pkg2name:
// ...
//sort data in a descending order of class length
data.sort((obja, objb) => objb["classes"].length - obja["classes"].length);
var infolist=data.map(function(element) {

    var pkginfo=element["classes"].map(function(classinfo) {
        if(classinfo["comment"]!=null) {
        return '<ul class="classi">'+
        '<li>'+'Classname: '+classinfo["classname"]+'</li>'+'<ul>'+
        '<li>'+'Comment: '+classinfo["comment"]+'</li>'+
        '<li>'+'Method number: '+classinfo["methodnum"]+'</li>'+
        '<li>'+'Line number: '+classinfo["linenum"]+'</li>'+'</ul>'+
        '</ul>'
        }
        else{
            return '<ul class="classi">'+
            '<li>'+'Classname: '+classinfo["classname"]+'</li>'+'<ul>'+
            '<li>'+'Method number: '+classinfo["methodnum"]+'</li>'+
            '<li>'+'Line number: '+classinfo["linenum"]+'</li>'+'</ul>'+
            '</ul>'
        }

    }).join(' ');
       return '<ul>' +
    '<li class="pkginfo">'+'<a class="pkgn" id='+element["pkgname"]+'>'+'Package name:'+ element["pkgname"]+'</a><div class="classmodel">'+pkginfo+'</div>' 
    +'</li>'
    +'</ul>'
}).join(' ');
//
console.log("information to show is "+infolist);
//demonstrate the information
document.getElementById("info").innerHTML=infolist;
