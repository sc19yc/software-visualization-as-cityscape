// import main Three.js
import * as THREE from './three.js-r123/build/three.module.js';
// import Three.js extension lib
import { OrbitControls } from './three.js-r123/examples/jsm/controls/OrbitControls.js';

// set the the size of canvas to show the model with width and height element
//the canvas is 70% width and window's height area

var width = window.innerWidth*0.7; 
var height = window.innerHeight; 
/**
* Camera Setting
*/
var k = width / height; //Three.js width and height ratio
var s = 5000; //control the area of camera render，the bigger of s，the larger of the camera render area

// -s * k, s * k, s, -s, 1, 2000 define a rendering rectangular space, the model outside of the space would not be rendered 
var camera = new THREE.OrthographicCamera(-s * k, s * k, s, -s, 1, 20000);
camera.position.set(-2000, 2000, -2000); //camera location setting
camera.lookAt(0, 0, 0); //camera lookat(face) position
/**
 * Renderer setting
 */
var renderer = new THREE.WebGLRenderer({
    antialias: true, 
    alpha: true,
});
renderer.setPixelRatio(window.devicePixelRatio);//Setting pixel ratio, keep the model demonstrated clearly
renderer.setSize(width, height); //set the area of render
// renderer.domElement is the outcome of render---model to demonstrate
document.getElementById('model').appendChild(renderer.domElement);// to show the model in the model id div


//create controls object
//to listen the mouse action
//Rotate the model : keep clicking mouse leftbutton and move it
//Move the model: keep clicking mouse rightbutton and move it
//Zoom the model: scroll mouse centerbutton
var controls = new OrbitControls(camera, renderer.domElement);

export { renderer, camera };