import * as THREE from '../three.js-r123/build/three.module.js';
import { districts } from '../LocationSet.js';

/**
 * mesh setting
 */

/**
 * Visualization
 * District:2
 * A:3 building //size of each building 5 30,6 32 ,7 80
 * B:10 building
 */


/*
District : 1
Building : 3
   line_num : 80 method_num : 2
   70,3
   90,5
*/

// Material
var materialb = new THREE.MeshPhongMaterial({
  color: 0x1C1C1C,
  specular: 0x5C5C5C,
  shininess: 30,
  side: THREE.DoubleSide,
});

var materiald = new THREE.MeshLambertMaterial({
  color: 0xADADAD,
  side: THREE.DoubleSide,
});

var meshes = new Array();
districts.sort((obja, objb) => objb.size - obja.size);


var district_gap = 20;
if (districts[districts.length - 1].size < 20) {
  district_gap = districts[districts.length - 1].size;
}
districtMove(districts, 0, 0, 0, 0, 0, district_gap);
setBuildingsMesh(districts);


function setBuildingsMesh(districts) {
  for (var j = 0; j < districts.length; j++) {
    var district = districts[j];
    console.log("District no. " + j + " modeling");
    var disgeo = new THREE.PlaneGeometry(district.size, district.size);
    disgeo.rotateX(Math.PI / 2);//the plane is vertical so rotate it 90 degree

    //cuz the coordinate represent the center of the district model ,but the designed algorithum thought coordinate as the corner
    //so we should move every district by half of its size 
    //move district plane down 1 unit=>ensure district plane model and building box model would not overlapped
    disgeo.translate(district.positionx + district.size / 2, -1, district.positiony + district.size / 2);
    var dismesh = new THREE.Mesh(disgeo, materiald);
    meshes.push(dismesh);
    for (var i = 0; i < district.buildings.length; i++) {
      //build building
      console.log("height is" + district.buildings[i].height);
      var geometry = new THREE.BoxGeometry(district.buildings[i].size, district.buildings[i].height, district.buildings[i].size);
      //The axis is core axis, but the calculated position axis is left top corner, so move half size for x and z position axis.
      geometry.translate(district.buildings[i].positionx +
        district.positionx +
        district.buildings[i].size / 2, district.buildings[i].height / 2, district.buildings[i].positiony +
        district.positiony + district.buildings[i].size / 2);
      var m = materialb;
      var mesh = new THREE.Mesh(geometry, m);

      meshes.push(mesh);
    }


  }
  return function () {
    return geometry, disgeo;
  }
}

//direction:right :0 or bottom:1
//distance
function districtMove(districts, index, length, lastlength, currentcol, currentver, gap) {
  if (index == 0) {
    districts[index].positionx = 0;
    districts[index].positiony = 0;
    //currentcol currentver initialization do not change
    length += districts[index].size;
    lastlength = districts[index].size;
  }
  else if (index % 2 == 0) {//even put bottom side
    if (currentcol + gap + districts[index].size > lastlength) //
    {
      districts[index].positionx = currentcol;
      districts[index].positiony = lastlength + gap;
      lastlength = length;
      // length += districts[index].size+gap;odd index would put in right side, length count first and definitely bigger than bottom side
      currentcol = 0;
      currentver = 0;
    }
    else if (currentcol != 0) {
      districts[index].positionx = currentcol;
      districts[index].positiony = lastlength + gap;
      currentcol += gap + districts[index].size; // count: next model coordinate x
    }
    else {
      districts[index].positionx = currentcol;
      districts[index].positiony = lastlength + gap;
      currentcol += gap + districts[index].size;
      if (index == districts.length - 1) {
        console.log(index + "size is: " + districts.legnth)
        length += districts[index].length + gap;
      }
    }
  }
  else
  //odd put right side
  {
    if (currentver + gap + districts[index].size > lastlength) //
    {
      districts[index].positionx = length + gap;
      districts[index].positiony = currentver;
      lastlength = length;
      length += districts[index].size + gap;
      currentcol = 0;
      currentver = 0;
    }
    else if (currentver != 0) {
      districts[index].positionx = length + gap;
      districts[index].positiony = currentver;
      currentver += gap + districts[index].size; // count
    }
    else {
      //currentver==0
      districts[index].positionx = length + gap;
      districts[index].positiony = currentver;
      currentver += gap + districts[index].size;
      console.log(index + "&&&" + districts.length);
      if (index == districts.length - 1) {
        length += districts[index].length + gap;
      }

    }

  }
  console.log("No " + index + " district size model : " + districts[index].size + "lastlength is " + lastlength + "length is " + length);
  if (index != districts.length - 1) {
    districtMove(districts, index + 1, length, lastlength, currentcol, currentver, gap);
  }

}


export { meshes }