
import * as THREE from '../three.js-r123/build/three.module.js';
import {meshes} from './mesh.js';
/**
 * Scene setting
 */
var scene = new THREE.Scene();
for (var m=0; m<meshes.length;m++)
{
    scene.add(meshes[m]);
}

scene.add(meshes[0]);
scene.add(meshes[1]);


/**
* Light setting
*/
// Parallel lights 1 2 3
var directionalLight = new THREE.DirectionalLight(0xffffff, 0.6);
directionalLight.position.set(400, 200, 300);
scene.add(directionalLight);

var directionalLight2 = new THREE.DirectionalLight(0xffffff, 0.6);
directionalLight2.position.set(-400, -200, -300);
scene.add(directionalLight2);

var directionalLight3 = new THREE.DirectionalLight(0xffffff, 0.6);
directionalLight3.position.set(400, -200, 300);
scene.add(directionalLight3);
//Environment light
var ambient = new THREE.AmbientLight(0xffffff, 0.6);
scene.add(ambient);


export { scene };