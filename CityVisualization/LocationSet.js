
/**
 * package => district
 * class => building
 * method num => building height
 * class line num => building size
 * 
 */


//A building is a cube
//building class: 
//size is the length of the bottom square of the building cube
//height is the height of the building cube
//(positionx,positiony) is the bottom side center coordinate of the building cube
class Building {
    size =0;
    height =0;
    positionx = 0;
    positiony = 0;
    constructor(line_num, method_num) {
        this.size = line_num;
        this.height = method_num;
    }
}


//create plane for districts
//building_num is the number of the buildings it have
//buildings is the buildings it have
//size is the side length of the district plane
//positionx 
class District {
    building_num = 0;
    buildings = new Array();
    size = 0;
    positionx = 0;
    positiony = 0;
    constructor() {
 
    }
    add(building) {
        this.buildings.push(building);
        this.building_num++;
    }
}
//prepare array to save the json info
//parse json
var data = $.parseJSON($.ajax({
    url: "AnalysisInfo.json",//json file path + name
    dataType: "json", 
    async: false
}).responseText);
console.log(data);
var districts = new Array();

for(var i=0;i<data.length;i++)
{
    setDistrict(data[i]["classes"]);
}

function setBuilding(building_size, building_height) {
    var b = new Building(building_size, building_height);
    var a = new Array();
    b.buildings = a;
    return function () {
        console.log("set building size:" + b.size + " hight:" + b.height);
        return {b};
    }

}



//set District variables and building location
//Compute District size
//district is an array of dict: {"linenum": , "methodnum":} 
function setDistrict(district) {
    //create a district
    var d = new District();
    //sort the array in a decesending order according to the array size
    district.sort((obja, objb) => objb.size - obja.size);
    for (var index = 0; index < district.length; index++) {
        var b = new Building(district[index].linenum, district[index].methodnum*100);//build building
        //should investigate avarage method num     
        //index is even, put it in the bottom side
        //if bottom colength，currentcol=0 first add verlength
        //if right verlength，currentver=0 first add colength
        console.log("district add building : size: "+ b.size+" height: "+b.height);
        //add a building to the building array of its belonging district
        d.add(b); 
        }
        //sort the buildings of this(same) district with bottom side size in descending order
        d.buildings.sort((obja,objb)=>objb.size-obja.size);
        var gap=5;
        if(d.buildings[d.building_num-1].size<5) {
            gap=d.buildings[d.building_num-1].size;
        }
        console.log("mini building size"+d.buildings[d.building_num-1].size+"gap is "+gap);
        //compute the space needed to arrange the buildings => district size
        var length=BuildingMove(d.buildings,0,d.buildings.length,0,0,0,gap)+gap;
    districts.push(d);
    d.size=length;
    //closure: keep district object in the memory
        return function () {
            return d;
        }
    }



//arrange the building in the district
function BuildingMove (buildings,index,length,lastlength,currentcol,currentver,gap)
{
    //when the building to set is the first one of the array
  if(index == 0)
  {
    buildings[index].positionx = 0;
    buildings[index].positiony = 0;
    //currentcol currentver initialization do not change
    length +=buildings[index].size;
    lastlength=buildings[index].size;
  }
  //when the index of the building to set is even excluding 0
  else if (index % 2 == 0) {//even put bottom side
    if (currentcol +gap+ buildings[index].size > lastlength) //
    {
        buildings[index].positionx = currentcol;
        buildings[index].positiony = lastlength+gap;
        //cuz the width of current column of buildings set is beyond the last column
        //so a new bottom side and a right side will be arranged 
        // the following buildings will begin to be arranged from the new sides
        lastlength=length;
        if(index==buildings.length-1){
            length+=buildings[index].size+gap
        }
        currentcol = 0;
        currentver = 0;
    }
    else if(currentcol!=0) {
        //   geometry.translate(currentcol+gap, 0, length+gap);// Move
        buildings[index].positionx = currentcol;
        buildings[index].positiony = lastlength+gap;
        currentcol += buildings[index].size; // Compute the next loc to set model
    }
    else {
        buildings[index].positionx = currentcol;
        buildings[index].positiony = lastlength + gap;
      currentcol += buildings[index].size+gap;
      length+= buildings[index].size+gap;
      
    }
}
else
//odd put right side
{
    if (currentver  +gap+ buildings[index].size > lastlength) {
        buildings[index].positionx = lastlength+gap;
        buildings[index].positiony = currentver;
        // meshes[j] = new THREE.Mesh(geometry, material); //Mesh
        lastlength=length;
        if(index==buildings.length-1){
            length+=buildings[index].size+gap
        }
        currentcol = 0;
        currentver = 0;
    }
    else if(currentver!=0) {
    //ordinary arrangement
        buildings[index].positionx = lastlength +gap;
        buildings[index].positiony = currentver;
        currentver += buildings[index].size+gap; // Compute the next loc
    }
    else {
    //currentver==0 set the first building of every column
        buildings[index].positionx = lastlength + gap;
        buildings[index].positiony = currentver;
        currentver += buildings[index].size+gap;
        length +=buildings[index].size+gap;

    }
    
}
console.log("No "+index+" district model size : "+buildings[index].size +" lastlength is "+lastlength + " length is "+length);
if(index!=buildings.length-1){
  length=BuildingMove(buildings,index+1,length,lastlength,currentcol,currentver,gap);
}
console.log("length is "+length);
return length;
}
export { districts };