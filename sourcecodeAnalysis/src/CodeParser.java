
import com.thoughtworks.qdox.JavaProjectBuilder;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaMethod;
import com.thoughtworks.qdox.model.JavaPackage;
import com.thoughtworks.qdox.model.JavaSource;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;

/**
 * 
 * @author Yingning Chen
 * 
 * Nesting level => Color saturation
 * Classes => Buildings
 * Districts => Packages
 * Number of methods => building height
 * Number of lines => building size
 *
 */

//Use Qdox framework to parse software based java
public class CodeParser {
	

    public static void main(String[] args) {
    	
    	String SourceCodePath = "";
    	//Read file to visual path configured in SourcePath.txt
    	try {
			BufferedReader fr=new BufferedReader(new FileReader("./SourcePath.txt"));
			SourceCodePath = fr.readLine();
			fr.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
        //this service should return a json text
        JSONArray returnJson = new JSONArray();
		JavaProjectBuilder builder = new JavaProjectBuilder();	
		builder.addSourceTree( new File( SourceCodePath ));
		//total packages
		Collection<JavaPackage> pkgs = builder.getPackages();
//
        try {
    		for(JavaPackage pkg : pkgs) {
    			JSONObject singleJson = new JSONObject();
    			//classes in each packages
    			Collection<JavaClass> classs  = pkg.getClasses();
    			singleJson.put("pkgname",pkg.getName());
    			singleJson.put("classesnum",classs.size());
    			//use loop in each classes to get method num and line num of classes
    			JSONArray classarray = new JSONArray();
    			for(JavaClass cls: classs) {	
    				JSONObject clas = new JSONObject();
    				clas.put("classname", cls.getCanonicalName());
    				clas.put("comment", cls.getComment());
    				Collection<JavaMethod> methods = cls.getMethods();
    				int linenum = cls.getLineNumber();
    				clas.put("methodnum",methods.size());
    				clas.put("linenum",linenum);
    				classarray.put(clas);
    			}
    			singleJson.put("classes",classarray);
    			returnJson.put(singleJson);
    		}
        }
        catch (Exception error) {
          error.printStackTrace();
        }
		System.out.println(returnJson.toString());
		
        try {
			Writer writer = new FileWriter("../CityVisualization/AnalysisInfo.json");
			writer.write(returnJson.toString());
			writer.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    }
    }