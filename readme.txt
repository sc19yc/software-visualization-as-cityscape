This is a software visualization project with two parts.
You should first run the analysis part to export the json file and then run the visualization part to visualize.


For analysis part:
The suitable java version is java 1.8.
Make sure your java verion in your computer environment is java 1.8.
Open terminal and enter java -version to see the version of java in the environment.

A runnable jar has been export that easy to run: sourcecodeAnalysis/runnablejar/analysis.jar

Run:
1. Set the project to analyze: go to sourcecodeAnalysis/runnablejar/ and Replace the content of SourcePath.txt by the project local address
2. open the terminal and go to sourcecodeAnalysis/runnablejar/
3. use commend : java -jar analysis.jar or double click the file.
4. move the output file Analysisinfo.json to the CityVisualization/ dir for the subsequent visualization part


For visualization part:
Make sure python environment has been implemented and configured in the environment in your computer
open the terminal and enter python to see the version of the python in your computer

For MacOS user and Windows user:
 (MacOS computer has original python2.7 environment)

Run:

1.open the terminal and go to CityVisualization/
2.use commend to run the server : 
python2 : python -m SimpleHTTPServer 8080
python3 : python -m http.server 8080
3.open the web brouser and input localhost:8080 to see the visualization outcome (Chrome is suggested)

